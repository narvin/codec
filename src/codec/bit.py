"""Tools for working with bits and bytes."""

import functools
from collections.abc import Iterable, Iterator

from codec import itr


def bits_to_int(bits: Iterable[int]) -> int:
    """Combine bits into an int.

    Args:
        bits: The bits of an int. Each item should be 0 or 1, with the most significant bit first.

    Returns:
        An int whose bits are ``bits``.
    """
    return functools.reduce(lambda a, c: a << 1 | c, bits)


def bitstream(bs: Iterable[int]) -> Iterator[int]:
    """Return the bits in an iterable of bytes.

    Args:
        bs: The values to decompose into bits. Each item should be between 0 and 255 (inclusive).
            Values outside of this range will be truncated, and only their least significant 8
            bits will be returned. ``bs`` could be a ``bytes``, or a ``bytearray``.

    Returns:
        An iterable that is the bit representation of ``bs``. Each item is a 0 or 1 bit. Every 8
        bits is a byte representation of an item in ``bs``.
    """
    return (b >> shift & 1 for b in bs for shift in range(7, -1, -1))


def bytestream(bits: Iterable[int]) -> Iterator[int]:
    """Return the bytes from an iterable of bits.

    Args:
        bits: The bits to compose into bytes. Each item should be 0 or 1, with the most
            significant bit first. Every 8 bits should represent a byte. If there aren't a
            multiple of 8 bits, the least significant bits of the last byte will be filled with
            0s to make the final full byte.

    Returns:
        An iterable that is the byte representation of ``bits``. Each item is a byte value
        between 0 and 255 (inclusive). Every 8 bits of ``bits`` is represented by a byte.
    """
    return (bits_to_int(byte) for byte in itr.batchf(bits, 8, 0))
