"""codec module."""

import functools
import itertools
from collections.abc import Iterable

from PIL import Image

from codec import lsbp

RGB_BYTES_PER_PIXEL = 3


def pad_bytes(  # noqa: PLR0913
    bs: Iterable[int],
    width: int,
    height: int,
    lsbp_padding: int,
    bytes_per_pixel: int,
    padding: bytes = b"\x00",
) -> bytes:
    """LSPB encode, and pad bytes to the amount of pixels for an image of the given dimensions.

    The first few bytes form a header that encodes the length of the header + padding. The length
    of the header itself is the :func:`~codec.lsbp.enclen` needed to LSBP encode 4 bytes with a
    padding of ``lsbp_padding``.

    Returns the header + padding + data, where the header and data are LSBP encoded.
    """
    header_bytes = lsbp.enclen(4, lsbp_padding)
    lsbp_bs = bytes(lsbp.enc(bs, lsbp_padding))
    overflow_bytes = (header_bytes + len(lsbp_bs)) % bytes_per_pixel
    alignment_bytes = bytes_per_pixel - overflow_bytes if overflow_bytes > 0 else 0
    header_alignment_bs_pixels = (header_bytes + alignment_bytes + len(lsbp_bs)) // bytes_per_pixel
    overflow_pixels = header_alignment_bs_pixels % width
    w_padding_pixels = width - overflow_pixels if overflow_pixels > 0 else 0
    h_padding_pixels = (
        max(height - ((header_alignment_bs_pixels + w_padding_pixels) // width), 0) * width
    )
    padding_pixels = w_padding_pixels + h_padding_pixels
    padding_bytes = padding_pixels * bytes_per_pixel + alignment_bytes
    num_padding, num_padding_bytes = divmod(padding_bytes, len(padding))
    return (
        bytes(lsbp.enc((header_bytes + padding_bytes).to_bytes(4), lsbp_padding))
        + padding * num_padding
        + padding[:num_padding_bytes]
        + lsbp_bs
    )


def unpad_bytes(bs: Iterable[int], lsbp_padding: int) -> bytes:
    """Return just the data from bytes padded with :func:`pad_bytes`."""
    it = iter(bs)
    header_bytes = lsbp.enclen(4, lsbp_padding)
    header = lsbp.dec(itertools.islice(it, header_bytes), lsbp_padding)
    drop = functools.reduce(lambda acc, curr: (acc << 8) + curr, header) - header_bytes
    next(itertools.islice(it, drop, drop), None)
    return bytes(lsbp.dec(it, lsbp_padding))


def bytes_to_png(bs: Iterable[int], width: int, height: int, lsbp_padding: int, path: str) -> int:
    """Save LSBP encoded bytes as a PNG with the given dimensions.

    The bytes will be processed with :func:`pad_bytes`, and the result will be saved as a PNG.
    """
    padded_bs = pad_bytes(bs, width, height, lsbp_padding, RGB_BYTES_PER_PIXEL)
    png = Image.frombytes("RGB", (width, height), padded_bs)
    png.save(path)
    return len(padded_bs)


def img_to_bytes(path: str, lsbp_padding: int) -> bytes:
    """Return the pixels of an image created with :func:`bytes_to_png`."""
    im = Image.open(path)
    data = im.getdata()
    return unpad_bytes(bytes(c for p in data for c in p), lsbp_padding)
