""":mod:`~codec.lsbp` unit tests."""

from collections.abc import Iterable, Iterator

import pytest

from codec import lsbp

# ruff: noqa: S101


def _inps() -> Iterator[bytes]:
    """Inputs for the tests."""
    inp = b"abcdefghijklmnop"
    return (inp[:end] for end in range(1, len(inp) + 1))


@pytest.mark.parametrize(
    ("bs", "padding"), [(inp, padding) for inp in _inps() for padding in range(1, 8)]
)
def test_enc_dec(bs: Iterable[int], padding: int) -> None:
    """Test :func:`~codec.lsbp.enc` and :func:`~codec.lsbp.dec` roundtrip."""
    assert list(lsbp.dec(lsbp.enc(bs, padding), padding)) == list(bs)


@pytest.mark.parametrize(
    ("bs", "padding", "mult"),
    [(inp, padding, mult) for inp in _inps() for padding in range(1, 8) for mult in [-1, 1]],
)
def test_enc_dec_noise(bs: Iterable[int], padding: int, mult: int) -> None:
    """Test :func:`~codec.lsbp.enc` and :func:`~codec.lsbp.dec` roundtrip with noise."""
    noise = (1 << (padding - 1)) - 1
    enc_bs = lsbp.enc(bs, padding)
    noisy_bs = [b + noise * mult for b in enc_bs]
    assert list(lsbp.dec(noisy_bs, padding)) == list(bs)


@pytest.mark.parametrize(
    ("bs", "padding"), [(inp, padding) for inp in _inps() for padding in range(1, 8)]
)
def test_enclen(bs: Iterable[int], padding: int) -> None:
    """Test :func:`~codec.lsbp.enclen`."""
    bs = list(bs)
    n = len(bs)
    exp = len(list(lsbp.enc(bs, padding)))
    assert lsbp.enclen(n, padding) == exp
