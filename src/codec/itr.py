"""More iter tools."""

import itertools
from collections.abc import Iterable
from typing import TypeVar

T = TypeVar("T")


def batch(xs: Iterable[T], size: int) -> Iterable[tuple[T, ...]]:
    """Batch an iterable into tuples of size ``size``.

    If there aren't enough items in ``xs``, the last underfull tuple is dropped.
    """
    its = [iter(xs)] * size
    return zip(*its, strict=False)


def batchf(xs: Iterable[T], size: int, fillvalue: T) -> Iterable[tuple[T, ...]]:
    """Batch an iterable into tuples of size ``size``.

    If there aren't enough items in ``xs``, the last underfull tuple is padded with ``fillvalue``.
    """
    its = [iter(xs)] * size
    return itertools.zip_longest(*its, fillvalue=fillvalue)
