"""Least Significant Bit Padding (LSBP) codec.

This encoding resists noise in the least significant bits of data bytes. The value of the padding
determines how many of the least significant bits of each 8-bit byte will be ignored in the
encoded data. If 1 point of noise changes the value of a byte by 1, then 2 bits of padding are
needed to recover the original value. To recover from up to 3 points of noise, 3 bits of padding
are needed. In general, the amount of noise that can be tolerated is ``(1 << (padding - 1)) - 1``.

The amount of padding increases the relative size of the encoded data; e.g, 4 bits of padding
will increase the size of the encoded data by 100%.
"""

import itertools
from collections.abc import Iterable, Iterator

from codec import itr
from codec.bit import bits_to_int, bitstream, bytestream


def _validate_padding(padding: int) -> None:
    size_byte = 8
    if padding < 1 or padding > size_byte - 1:
        msg = f"invalid padding {padding}"
        raise ValueError(msg)


def enc(bs: Iterable[int], padding: int) -> Iterator[int]:
    """LSBP encode an iterable of byte values.

    Args:
        bs: The byte values to encode. Each item should be between 0 and 255 (inclusive). ``bs``
            could be a ``bytes``, or a ``bytearray``.
        padding: The number of least significant bits to pad with 0s in each output byte.
            ``padding`` must be between 1 and 7 (inclusive).

    Returns:
        An iterable of byte values where the ``padding`` least significant bits of each value are
        0. The non-padding bits taken sequentially form the bytes from ``bs``.
    """
    _validate_padding(padding)
    return (bits_to_int(chunk) << padding for chunk in itr.batchf(bitstream(bs), 8 - padding, 0))


def dec(bs: Iterable[int], padding: int) -> Iterator[int]:
    """LSBP decode an iterable of byte values.

    ``1 << padding`` is the resolution, so each value in ``bs`` is first corrected by rounding it
    to the nearest multiple of the resolution, which also zeroes out the ``padding`` least
    significant bits.

    Args:
        bs: The byte values to decode. ``bs`` should be LSBP encoded, and each value can have up
            to ``(1 << (padding - 1)) - 1`` of noise. ``bs`` could be a ``bytes``,
            or a ``bytearray``.
        padding: The number of least significant bits that are padded with 0s in each byte from
            ``bs``. This should be the same value that was used to encode ``bs``, and must be
            between 1 and 7 (inclusive).

    Returns:
        An iterable of byte values where noise has been removed , and the ``padding`` least
        significant bits of each value have been restored.
    """
    _validate_padding(padding)
    resolution = 1 << padding
    corrected_bs = (round(b / resolution) * resolution for b in bs)
    bits_no_padding = (
        bit_no_padding
        for byte_bits in itr.batch(bitstream(corrected_bs), 8)
        for bit_no_padding in itertools.islice(byte_bits, 8 - padding)
    )
    trim_bits_no_padding = (bit for byte_bits in itr.batch(bits_no_padding, 8) for bit in byte_bits)
    return bytestream(trim_bits_no_padding)


def enclen(n: int, padding: int) -> int:
    """Return the number of bytes required to LSBP encode ``n`` bytes with the given ``padding``.

    ``padding`` must be between 1 and 7 (inclusive).
    """
    _validate_padding(padding)
    a, b = divmod(n * 8, 8 - padding)
    return a + (1 if b > 0 else 0)
