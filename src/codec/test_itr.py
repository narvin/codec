""":mod:`~codec.itr` unit tests."""

from collections.abc import Iterable

import pytest

from codec import itr

# ruff: noqa: S101


@pytest.mark.parametrize(
    ("xs", "size", "exp"),
    [
        pytest.param(
            [1, 2, 3, 4, 5, 6, 7, 8],
            1,
            [(1,), (2,), (3,), (4,), (5,), (6,), (7,), (8,)],
            id="singles",
        ),
        pytest.param(
            [1, 2, 3, 4, 5, 6, 7, 8], 2, [(1, 2), (3, 4), (5, 6), (7, 8)], id="no remainder"
        ),
        pytest.param([1, 2, 3, 4, 5, 6, 7, 8], 3, [(1, 2, 3), (4, 5, 6)], id="remainder"),
        pytest.param([1, 2, 3, 4, 5, 6, 7, 8], 8, [(1, 2, 3, 4, 5, 6, 7, 8)], id="full"),
        pytest.param([1, 2, 3, 4, 5, 6, 7], 8, [], id="underfull"),
        pytest.param([1, 2, 3, 4, 5, 6, 7, 8, 9], 8, [(1, 2, 3, 4, 5, 6, 7, 8)], id="overfull"),
    ],
)
def test_batch(xs: Iterable[itr.T], size: int, exp: Iterable[tuple[itr.T, ...]]) -> None:
    """Test :func:`~codec.itr.batch`."""
    act = itr.batch(xs, size)
    assert list(act) == exp


@pytest.mark.parametrize(
    ("xs", "size", "exp"),
    [
        pytest.param(
            [1, 2, 3, 4, 5, 6, 7, 8],
            1,
            [(1,), (2,), (3,), (4,), (5,), (6,), (7,), (8,)],
            id="singles",
        ),
        pytest.param(
            [1, 2, 3, 4, 5, 6, 7, 8], 2, [(1, 2), (3, 4), (5, 6), (7, 8)], id="no remainder"
        ),
        pytest.param(
            [1, 2, 3, 4, 5, 6, 7, 8], 3, [(1, 2, 3), (4, 5, 6), (7, 8, 0)], id="remainder"
        ),
        pytest.param([1, 2, 3, 4, 5, 6, 7, 8], 8, [(1, 2, 3, 4, 5, 6, 7, 8)], id="full"),
        pytest.param([1, 2, 3, 4, 5, 6, 7], 8, [(1, 2, 3, 4, 5, 6, 7, 0)], id="underfull"),
        pytest.param(
            [1, 2, 3, 4, 5, 6, 7, 8, 9],
            8,
            [(1, 2, 3, 4, 5, 6, 7, 8), (9, 0, 0, 0, 0, 0, 0, 0)],
            id="overfull",
        ),
    ],
)
def test_batchf(xs: Iterable[itr.T], size: int, exp: Iterable[tuple[itr.T, ...]]) -> None:
    """Test :func:`~codec.itr.batchf`."""
    act = itr.batchf(xs, size, fillvalue=0)
    assert list(act) == exp
