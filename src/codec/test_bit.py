""":mod:`~codec.bit` unit tests."""

from collections.abc import Iterable, Iterator

import pytest

from codec import bit

# ruff: noqa: S101


@pytest.mark.parametrize(
    ("bits", "exp"),
    [
        pytest.param([0, 0, 0, 0, 0, 0, 0, 0], 0, id="8-bit no 1s"),
        pytest.param([0, 0, 0, 0, 0, 0, 0, 1], 1, id="8-bit last bit 1"),
        pytest.param([0, 0, 0, 0, 0, 1, 0, 1], 5, id="8-bit 2nd to last and last bit 1"),
        pytest.param([1, 1, 1, 1, 1, 1, 1, 1], 255, id="8-bit all 1s"),
        pytest.param([0] * 24 + [0, 0, 0, 0, 0, 0, 0, 0], 0, id="32-bit no 1s"),
        pytest.param([0] * 24 + [0, 0, 0, 0, 0, 0, 0, 1], 1, id="32-bit last bit 1"),
        pytest.param(
            [0] * 24 + [0, 0, 0, 0, 0, 1, 0, 1], 5, id="32-bit 2nd to last and last bit 1"
        ),
        pytest.param([0] * 24 + [1, 1, 1, 1, 1, 1, 1, 1], 255, id="32-bit last 8 bits 1"),
        pytest.param([1, 1, 1, 1, 1, 1, 1, 1] * 4, 2**32 - 1, id="32-bit all 1s"),
        pytest.param(b"\x01\x00\x01\x01", 0b1011, id="bytes input"),
        pytest.param(bytearray(b"\x01\x00\x01\x01"), 0b1011, id="bytearray input"),
        pytest.param(map(lambda _: 1, range(8)), 0b11111111, id="map input"),  # noqa: C417
    ],
)
def test_bit_to_int(bits: Iterable[int], exp: int) -> None:
    """Test :func:`~codec.bit.bit_to_int`."""
    act = bit.bits_to_int(bits)
    assert act == exp


@pytest.mark.parametrize(
    ("bs", "exp"),
    [
        pytest.param(b"", [], id="empty bytes"),
        pytest.param(b"\x00", [0, 0, 0, 0, 0, 0, 0, 0], id="0 byte"),
        pytest.param(b"\x0f", [0, 0, 0, 0, 1, 1, 1, 1], id="4 lsb 1s byte"),
        pytest.param(b"\xf0", [1, 1, 1, 1, 0, 0, 0, 0], id="4 msb 1s byte"),
        pytest.param(b"\xff", [1, 1, 1, 1, 1, 1, 1, 1], id="all 1s byte"),
        pytest.param(
            b"\x00\x0f\xff",
            [*[0, 0, 0, 0, 0, 0, 0, 0], *[0, 0, 0, 0, 1, 1, 1, 1], *[1, 1, 1, 1, 1, 1, 1, 1]],
            id="3 bytes",
        ),
        pytest.param(
            bytearray(b"\x00\x0f\xff"),
            [*[0, 0, 0, 0, 0, 0, 0, 0], *[0, 0, 0, 0, 1, 1, 1, 1], *[1, 1, 1, 1, 1, 1, 1, 1]],
            id="bytearray",
        ),
        pytest.param(
            map(lambda x: x, b"\x0f\xf0\xff"),  # noqa: C417
            [*[0, 0, 0, 0, 1, 1, 1, 1], *[1, 1, 1, 1, 0, 0, 0, 0], *[1, 1, 1, 1, 1, 1, 1, 1]],
            id="map byte input",
        ),
        pytest.param(
            map(lambda x: int(x), b"\x0f\xf0\xff"),  # noqa: C417
            [*[0, 0, 0, 0, 1, 1, 1, 1], *[1, 1, 1, 1, 0, 0, 0, 0], *[1, 1, 1, 1, 1, 1, 1, 1]],
            id="map int input",
        ),
        pytest.param(
            bit.bytestream([1, 0, 0, 0, 1, 0, 0, 1]),
            [1, 0, 0, 0, 1, 0, 0, 1],
            id="inverse of bytestream",
        ),
    ],
)
def test_bitstream(bs: Iterable[int], exp: Iterator[int]) -> None:
    """Test :func:`~codec.bit.bitstream`."""
    assert list(bit.bitstream(bs)) == list(exp)


@pytest.mark.parametrize(
    ("bits", "exp"),
    [
        pytest.param([], [], id="empty list"),
        pytest.param([0, 0, 0, 0, 1, 0, 1, 1], [0b1011], id="one byte"),
        pytest.param(
            [*[0, 0, 0, 0, 1, 0, 1, 1], *[0, 0, 0, 1, 1, 0, 0, 1], *[0, 0, 0, 0, 1, 0, 0, 0]],
            [0b1011, 0b11001, 0b1000],
            id="three bytes",
        ),
        pytest.param([0, 0, 0, 0, 1, 0, 1], [0b1010], id="underfull byte"),
        pytest.param(bit.bitstream(b"abc"), iter(b"abc"), id="inverse of bitstream"),
    ],
)
def test_bytestream(bits: Iterable[int], exp: Iterator[int]) -> None:
    """Test :func:`~codec.bit.bytestream`."""
    assert list(bit.bytestream(bits)) == list(exp)
