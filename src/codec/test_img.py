""":mod:`~codec.img` unit tests."""

from collections.abc import Iterable, Iterator

import pytest

from codec import img

# ruff: noqa: S101


def _inps() -> Iterator[bytes]:
    """Inputs for the tests."""
    inp = b"abcdefghijklmnop"
    return (inp[:end] for end in range(1, len(inp) + 1))


@pytest.mark.parametrize(
    ("bs", "lsbp_padding"), [(inp, lsbp_padding) for inp in _inps() for lsbp_padding in range(1, 8)]
)
def test_pad_unpad_bytes(bs: Iterable[int], lsbp_padding: int) -> None:
    """Test :func:`~codec.img.pad_bytes` and :func:`~codec.img.unpad_bytes` roundtrip."""
    assert img.unpad_bytes(img.pad_bytes(bs, 10, 2, lsbp_padding, 3), lsbp_padding) == bs
