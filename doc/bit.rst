***
bit
***

bit Module
==========

.. automodule:: codec.bit
   :members:

bit Unit Tests
==============

.. automodule:: codec.test_bit
   :members:
