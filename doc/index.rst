.. include:: ../README.rst

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   bit
   itr
   lsbp
   img


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
