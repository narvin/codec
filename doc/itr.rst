***
itr
***

itr Module
==========

.. automodule:: codec.itr
   :members:

itr Unit Tests
==============

.. automodule:: codec.test_itr
   :members:
