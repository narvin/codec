"""Build Cython modules."""

import setuptools
from Cython import Build  # type: ignore[import-untyped]

CYTHON_MODULES = ["src/codec/bit.py", "src/codec/img.py", "src/codec/itr.py", "src/codec/lsbp.py"]


def build_cython() -> int:
    """Build the extension modules."""
    setuptools.setup(
        script_args=["build_ext"],
        name="codec",
        ext_modules=Build.cythonize(CYTHON_MODULES, compiler_directives={"language_level": "3"}),
    )
    return 0


if __name__ == "__main__":
    raise SystemExit(build_cython())
